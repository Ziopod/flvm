---
title: Arnaud Houley
name: Arnaud Houley
role: 'Maître pâtissier, confiseur et chocolatier'
contact:
  - type: phone
    value: 06 64 50 71 23
  - type: email
    value: arnaud@flvm.fr
---


Arnaud aura plaisir à vous transmettre les secrets et astuces des grands pâtissiers.