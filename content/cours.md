---
title: Cours de cuisine
---
## Cours de pâtisserie

![mille-feuilles](cours/images/mille-feuilles.jpg)

Arnaud Houley, maître pâtissier, vous aidera à parfaire vos connaissances en pâtisserie, chocolaterie et confiserie.
Les cours se déroulent à [l'Atelier de pâtisserie](informations#l-atelier-de-patisserie).

Pour planifier votre cours, n'hésitez pas à contacter Arnaud par téléphone au 06 64 50 71 23 ou par courriel <contact@flvm.fr>

[Choisir un cours de pâtisserie](/planning){.btn .btn-cta}

## Cours de boulangerie 

![cours-boulange](cours/images/cours-boulange.jpg)

Arnaud Houley vous montrera le chemin de la panification et autres tours de mains propres aux boulangers, il vous acueillera dans le fournil du [160 rue Eau de Robec](informations#le-160).

Afin de planifier un cours de boulangerie, contactez Arnaud au 06 64 50 71 23.

[Choisir un cours de boulangerie](/planning){.btn .btn-cta}

## Cours de cuisine

![Découpage des échalottes](cours/images/cours-de-cuisine.jpg)

Marie Lionis vous accompagnera dans l'élaboration de recettes, elle vous donnera les trucs et astuces qui font la différence. Marie vous accueillera au [160 rue Eau de Robec](informations#le-160).

Afin de planifier un cours de cuisine, contactez Marie au 06 62 81 56 74.

[Choisir un cours de cuisine](/planning){.btn .btn-cta}