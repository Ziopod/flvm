---
title: FLVM
description: Des cours de cuisines et de pâtisseries pour toutes et tous!
published: '2019-10-29T00:00:00+01:00'
author: content/auteurs/flvm.md
heroes:
- title: Laisse ton cartable et mets ton tablier !
  visuel: "/uploads/les-petites-toques.jpg"
  alt: Des enfants en atelier de pâtisserie
  legend: "<h1>Laisse ton cartable et mets ton tablier !</h1><p>Viens me retrouver
    après la classe, <strong>à partir de 17h du lundi au vendredi</strong>. Nous apprendrons
    ensemble trucs et astuces de pâtissier et cuisinier !</p>"
  add_link_:
    text: Réserver un cours
    href: "/cours"
    color: "#F8E71C"
    icon: ''
additionnal_contents:
- template: section
  add_link_:
    icon: []
    color: "#4A90E2"
    text: ''
    href: ''
  title: Retrouvez-nous
  content: Partagez vos moments de cours sur [la page Facebook de FLVM](https://www.facebook.com/flvm.cours/)
  cover:
    src: ''
    alt: ''
- template: section
  add_link_:
    icon: ''
    color: "#F8E71C"
    text: Réserver un cours
    href: "/cours"
  title: Au 2 place du 39è Régiment d'Infanterie
  content: "# Au 2 place du 39è Régiment d'Infanterie\n\n## Atelier Viennoiserie\n\nConfectionnez
    les croissants et pains chocolat de votre enfance et autres gourmandises d'exception
    !\n\n## Atelier pâtisserie\n\nVenez apprendre à réaliser tous les gâteaux et desserts
    professionnels que vous aurez envie de refaire à la maison\n\n# "
  cover:
    src: "/uploads/montage-boulangerie-hd.jpg"
    alt: Miches de pain
- template: selectionner-un-intervenant
  intervenant: content/intervenants/arnaud-houley.md
- template: selectionner-un-intervenant
  intervenant: content/intervenants/example.md
related_contents:
- content/sections/cours/cours-de-pâtisserie.md
contenu_supplementaire:
- contenu: content/sections/cours/cours-de-pâtisserie.md
  lieu: content/lieux/le-160.md
  intervenant: content/intervenants/arnaud-houley.md
hero:
  visuel: "/uploads/les-petites-toques.jpg"
  alt: Des enfants en atelier de pâtisserie
  legend: "<h1>Laisse ton cartable et mets ton tablier !</h1><p>Viens me retrouver
    après la classe, <strong>à partir de 17h du lundi au vendredi</strong>. Nous apprendrons
    ensemble trucs et astuces de pâtissier et cuisinier !</p>"
  add_link_:
    text: ''
    href: ''
    icon: ''
    color: ''
hero_block:
- template: hero
  add_link_:
    icon: []
    color: "#4A90E2"
    text: ''
    href: ''
  visuel: ''
  alt: ''
  legend: ''
- template: hero
  add_link_:
    icon: []
    color: "#4A90E2"
    text: ''
    href: ''
  visuel: ''
  alt: ''
  legend: ''

---
## Team Building

FLVM, c’est aussi un lieu pour vos événements professionnels, n’hésitez pas à nous contacter à [contact@flmv.fr](mailto:contact@flmv.fr).

## Enterrement de vie de jeune fille

![eunes filles s'amusent en cuisine](images/enterrement-de-vie-de-jf.jpg "Enterrement de vie de jeune fille")

Faites une surprise à la future mariée et offrez-lui un cours mémorable de cuisine et de pâtisserie !

## Laisse ton cartable et mets ton tablier !

Viens me retrouver après la classe, à partir de 17h du lundi au vendredi. Nous apprendrons ensemble trucs et astuces de pâtissier et cuisinier ! Si tu veux, dès les mercredis matins, tu peux aussi réaliser du bon pain ou de la viennoiserie! MIAM 😋

[Réserver un cours](planning){.btn-cta}

### Un anniversaire original

invite tes copains pour réaliser la pâtisserie ou la cuisine de ton choix.

### Retrouvez-nous

Mettez les photos de vos cours et [vos commentaires sur Facebook](https://www.facebook.com/flvm.cours/)